/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package types_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gopkg.org/types"
)

func TestNewUnix(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		ts := types.NewUnix(time.Date(2023, 11, 10, 15, 30, 45, 987654321, time.UTC))
		assert.Equal(t, types.Unix(1699630245), ts)
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		ts := types.NewUnix(time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC))
		assert.Equal(t, types.Unix(0), ts)
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		ts := types.NewUnix(time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC))
		assert.Equal(t, types.Unix(-3600), ts)
	})
}

func TestUnix_UnixMilli(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMilli(1699630245000), types.Unix(1699630245).UnixMilli())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMilli(0), types.Unix(0).UnixMilli())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMilli(-3600000), types.Unix(-3600).UnixMilli())
	})
}

func TestUnix_UnixMicro(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMicro(1699630245000000), types.Unix(1699630245).UnixMicro())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMicro(0), types.Unix(0).UnixMicro())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMicro(-3600000000), types.Unix(-3600).UnixMicro())
	})
}

func TestUnix_UnixNano(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.UnixNano(1699630245000000000), types.Unix(1699630245).UnixNano())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixNano(0), types.Unix(0).UnixNano())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixNano(-3600000000000), types.Unix(-3600).UnixNano())
	})
}

func TestUnix_MarshalText(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		ts := types.Unix(1699630245)
		assert.Equal(t, time.Date(2023, 11, 10, 15, 30, 45, 0, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("1699630245"), data)
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		ts := types.Unix(0)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("0"), data)
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		ts := types.Unix(-3600)
		assert.Equal(t, time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("-3600"), data)
	})
}

func TestUnix_UnmarshalText(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		var ts types.Unix
		assert.NoError(t, ts.UnmarshalText([]byte("1699630245")))
		assert.Equal(t, types.Unix(1699630245), ts)
		assert.Equal(t, time.Date(2023, 11, 10, 15, 30, 45, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		var ts types.Unix
		assert.NoError(t, ts.UnmarshalText([]byte("0")))
		assert.Equal(t, types.Unix(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		var ts types.Unix
		assert.NoError(t, ts.UnmarshalText([]byte("-3600")))
		assert.Equal(t, types.Unix(-3600), ts)
		assert.Equal(t, time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("nil", func(t *testing.T) {
		var ts types.Unix
		assert.Error(t, ts.UnmarshalText(nil))
		assert.Equal(t, types.Unix(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("empty", func(t *testing.T) {
		var ts types.Unix
		assert.Error(t, ts.UnmarshalText([]byte("")))
		assert.Equal(t, types.Unix(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("invalid", func(t *testing.T) {
		var ts types.Unix
		assert.Error(t, ts.UnmarshalText([]byte("unknown")))
		assert.Equal(t, types.Unix(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})
}

func TestNewUnixMilli(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		ts := types.NewUnixMilli(time.Date(2023, 11, 10, 15, 30, 45, 987654321, time.UTC))
		assert.Equal(t, types.UnixMilli(1699630245987), ts)
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		ts := types.NewUnixMilli(time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC))
		assert.Equal(t, types.UnixMilli(0), ts)
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		ts := types.NewUnixMilli(time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC))
		assert.Equal(t, types.UnixMilli(-3600000), ts)
	})
}

func TestUnixMilli_Unix(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.Unix(1699630245), types.UnixMilli(1699630245987).Unix())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.Unix(0), types.UnixMilli(0).Unix())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.Unix(-3600), types.UnixMilli(-3600000).Unix())
	})
}

func TestUnixMilli_UnixMicro(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMicro(1699630245987000), types.UnixMilli(1699630245987).UnixMicro())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMicro(0), types.UnixMilli(0).UnixMicro())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMicro(-3600000000), types.UnixMilli(-3600000).UnixMicro())
	})
}

func TestUnixMilli_UnixNano(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.UnixNano(1699630245987000000), types.UnixMilli(1699630245987).UnixNano())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixNano(0), types.UnixMilli(0).UnixNano())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixNano(-3600000000000), types.UnixMilli(-3600000).UnixNano())
	})
}

func TestUnixMilli_MarshalText(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		ts := types.UnixMilli(1699630245987)
		assert.Equal(t, time.Date(2023, 11, 10, 15, 30, 45, 987000000, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("1699630245987"), data)
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		ts := types.UnixMilli(0)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("0"), data)
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		ts := types.UnixMilli(-3600000)
		assert.Equal(t, time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("-3600000"), data)
	})
}

func TestUnixMilli_UnmarshalText(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		var ts types.UnixMilli
		assert.NoError(t, ts.UnmarshalText([]byte("1699630245987")))
		assert.Equal(t, types.UnixMilli(1699630245987), ts)
		assert.Equal(t, time.Date(2023, 11, 10, 15, 30, 45, 987000000, time.UTC), ts.Time().UTC())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		var ts types.UnixMilli
		assert.NoError(t, ts.UnmarshalText([]byte("0")))
		assert.Equal(t, types.UnixMilli(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		var ts types.UnixMilli
		assert.NoError(t, ts.UnmarshalText([]byte("-3600000")))
		assert.Equal(t, types.UnixMilli(-3600000), ts)
		assert.Equal(t, time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("nil", func(t *testing.T) {
		var ts types.UnixMilli
		assert.Error(t, ts.UnmarshalText(nil))
		assert.Equal(t, types.UnixMilli(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("empty", func(t *testing.T) {
		var ts types.UnixMilli
		assert.Error(t, ts.UnmarshalText([]byte("")))
		assert.Equal(t, types.UnixMilli(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("invalid", func(t *testing.T) {
		var ts types.UnixMilli
		assert.Error(t, ts.UnmarshalText([]byte("unknown")))
		assert.Equal(t, types.UnixMilli(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})
}

func TestNewUnixMicro(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		ts := types.NewUnixMicro(time.Date(2023, 11, 10, 15, 30, 45, 987654321, time.UTC))
		assert.Equal(t, types.UnixMicro(1699630245987654), ts)
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		ts := types.NewUnixMicro(time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC))
		assert.Equal(t, types.UnixMicro(0), ts)
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		ts := types.NewUnixMicro(time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC))
		assert.Equal(t, types.UnixMicro(-3600000000), ts)
	})
}

func TestUnixMicro_Unix(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.Unix(1699630245), types.UnixMicro(1699630245987654).Unix())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.Unix(0), types.UnixMicro(0).Unix())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.Unix(-3600), types.UnixMicro(-3600000000).Unix())
	})
}

func TestUnixMicro_UnixMilli(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMilli(1699630245987), types.UnixMicro(1699630245987654).UnixMilli())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMilli(0), types.UnixMicro(0).UnixMilli())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMilli(-3600000), types.UnixMicro(-3600000000).UnixMilli())
	})
}

func TestUnixMicro_UnixNano(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.UnixNano(1699630245987654000), types.UnixMicro(1699630245987654).UnixNano())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixNano(0), types.UnixMicro(0).UnixNano())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixNano(-3600000000000), types.UnixMicro(-3600000000).UnixNano())
	})
}

func TestUnixMicro_MarshalText(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		ts := types.UnixMicro(1699630245987654)
		assert.Equal(t, time.Date(2023, 11, 10, 15, 30, 45, 987654000, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("1699630245987654"), data)
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		ts := types.UnixMicro(0)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("0"), data)
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		ts := types.UnixMicro(-3600000000)
		assert.Equal(t, time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("-3600000000"), data)
	})
}

func TestUnixMicro_UnmarshalText(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		var ts types.UnixMicro
		assert.NoError(t, ts.UnmarshalText([]byte("1699630245987654")))
		assert.Equal(t, types.UnixMicro(1699630245987654), ts)
		assert.Equal(t, time.Date(2023, 11, 10, 15, 30, 45, 987654000, time.UTC), ts.Time().UTC())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		var ts types.UnixMicro
		assert.NoError(t, ts.UnmarshalText([]byte("0")))
		assert.Equal(t, types.UnixMicro(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		var ts types.UnixMicro
		assert.NoError(t, ts.UnmarshalText([]byte("-3600000000")))
		assert.Equal(t, types.UnixMicro(-3600000000), ts)
		assert.Equal(t, time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("nil", func(t *testing.T) {
		var ts types.UnixMicro
		assert.Error(t, ts.UnmarshalText(nil))
		assert.Equal(t, types.UnixMicro(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("empty", func(t *testing.T) {
		var ts types.UnixMicro
		assert.Error(t, ts.UnmarshalText([]byte("")))
		assert.Equal(t, types.UnixMicro(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("invalid", func(t *testing.T) {
		var ts types.UnixMicro
		assert.Error(t, ts.UnmarshalText([]byte("unknown")))
		assert.Equal(t, types.UnixMicro(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})
}

func TestNewUnixNano(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		ts := types.NewUnixNano(time.Date(2023, 11, 10, 15, 30, 45, 987654321, time.UTC))
		assert.Equal(t, types.UnixNano(1699630245987654321), ts)
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		ts := types.NewUnixNano(time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC))
		assert.Equal(t, types.UnixNano(0), ts)
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		ts := types.NewUnixNano(time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC))
		assert.Equal(t, types.UnixNano(-3600000000000), ts)
	})
}

func TestUnixNano_Unix(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.Unix(1699630245), types.UnixNano(1699630245987654321).Unix())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.Unix(0), types.UnixNano(0).Unix())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.Unix(-3600), types.UnixNano(-3600000000000).Unix())
	})
}

func TestUnixNano_UnixMilli(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMilli(1699630245987), types.UnixNano(1699630245987654321).UnixMilli())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMilli(0), types.UnixNano(0).UnixMilli())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMilli(-3600000), types.UnixNano(-3600000000000).UnixMilli())
	})
}

func TestUnixNano_UnixMicro(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMicro(1699630245987654), types.UnixNano(1699630245987654321).UnixMicro())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMicro(0), types.UnixNano(0).UnixMicro())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		assert.Equal(t, types.UnixMicro(-3600000000), types.UnixNano(-3600000000000).UnixMicro())
	})
}

func TestUnixNano_MarshalText(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		ts := types.UnixNano(1699630245987654321)
		assert.Equal(t, time.Date(2023, 11, 10, 15, 30, 45, 987654321, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("1699630245987654321"), data)
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		ts := types.UnixNano(0)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("0"), data)
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		ts := types.UnixNano(-3600000000000)
		assert.Equal(t, time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC), ts.Time().UTC())
		data, err := ts.MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("-3600000000000"), data)
	})
}

func TestUnixNano_UnmarshalText(t *testing.T) {
	t.Parallel()

	t.Run("2023-11-10T15:30:45Z", func(t *testing.T) {
		var ts types.UnixNano
		assert.NoError(t, ts.UnmarshalText([]byte("1699630245987654321")))
		assert.Equal(t, types.UnixNano(1699630245987654321), ts)
		assert.Equal(t, time.Date(2023, 11, 10, 15, 30, 45, 987654321, time.UTC), ts.Time().UTC())
	})

	t.Run("1970-01-01T00:00:00Z", func(t *testing.T) {
		var ts types.UnixNano
		assert.NoError(t, ts.UnmarshalText([]byte("0")))
		assert.Equal(t, types.UnixNano(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("1969-12-31T23:00:00Z", func(t *testing.T) {
		var ts types.UnixNano
		assert.NoError(t, ts.UnmarshalText([]byte("-3600000000000")))
		assert.Equal(t, types.UnixNano(-3600000000000), ts)
		assert.Equal(t, time.Date(1969, 12, 31, 23, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("nil", func(t *testing.T) {
		var ts types.UnixNano
		assert.Error(t, ts.UnmarshalText(nil))
		assert.Equal(t, types.UnixNano(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("empty", func(t *testing.T) {
		var ts types.UnixNano
		assert.Error(t, ts.UnmarshalText([]byte("")))
		assert.Equal(t, types.UnixNano(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})

	t.Run("invalid", func(t *testing.T) {
		var ts types.UnixNano
		assert.Error(t, ts.UnmarshalText([]byte("unknown")))
		assert.Equal(t, types.UnixNano(0), ts)
		assert.Equal(t, time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC), ts.Time().UTC())
	})
}
