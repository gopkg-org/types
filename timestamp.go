/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package types

import (
	"strconv"
	"time"
)

func NewUnix(t time.Time) Unix {
	return Unix(t.Unix())
}

type Unix int64

func (ts Unix) UnixMilli() UnixMilli {
	return UnixMilli(ts * 1e3)
}

func (ts Unix) UnixMicro() UnixMicro {
	return UnixMicro(ts * 1e6)
}

func (ts Unix) UnixNano() UnixNano {
	return UnixNano(ts * 1e9)
}

func (ts Unix) Time() time.Time {
	return time.Unix(int64(ts), 0)
}

func (ts Unix) MarshalText() ([]byte, error) {
	return []byte(strconv.FormatInt(int64(ts), 10)), nil
}

func (ts *Unix) UnmarshalText(data []byte) error {
	val, err := strconv.ParseInt(string(data), 10, 64)
	if err != nil {
		return err
	}

	*ts = Unix(val)

	return nil
}

func NewUnixMilli(t time.Time) UnixMilli {
	return UnixMilli(t.UnixMilli())
}

type UnixMilli int64

func (ts UnixMilli) Unix() Unix {
	return Unix(ts / 1e3)
}

func (ts UnixMilli) UnixMicro() UnixMicro {
	return UnixMicro(ts * 1e3)
}

func (ts UnixMilli) UnixNano() UnixNano {
	return UnixNano(ts * 1e6)
}

func (ts UnixMilli) Time() time.Time {
	return time.UnixMilli(int64(ts))
}

func (ts UnixMilli) MarshalText() ([]byte, error) {
	return []byte(strconv.FormatInt(int64(ts), 10)), nil
}

func (ts *UnixMilli) UnmarshalText(data []byte) error {
	val, err := strconv.ParseInt(string(data), 10, 64)
	if err != nil {
		return err
	}

	*ts = UnixMilli(val)

	return nil
}

func NewUnixMicro(t time.Time) UnixMicro {
	return UnixMicro(t.UnixMicro())
}

type UnixMicro int64

func (ts UnixMicro) Unix() Unix {
	return Unix(ts / 1e6)
}

func (ts UnixMicro) UnixMilli() UnixMilli {
	return UnixMilli(ts / 1e3)
}

func (ts UnixMicro) UnixNano() UnixNano {
	return UnixNano(ts * 1e3)
}

func (ts UnixMicro) Time() time.Time {
	return time.UnixMicro(int64(ts))
}

func (ts UnixMicro) MarshalText() ([]byte, error) {
	return []byte(strconv.FormatInt(int64(ts), 10)), nil
}

func (ts *UnixMicro) UnmarshalText(data []byte) error {
	val, err := strconv.ParseInt(string(data), 10, 64)
	if err != nil {
		return err
	}

	*ts = UnixMicro(val)

	return nil
}

func NewUnixNano(t time.Time) UnixNano {
	return UnixNano(t.UnixNano())
}

type UnixNano int64

func (ts UnixNano) Unix() Unix {
	return Unix(ts / 1e9)
}

func (ts UnixNano) UnixMilli() UnixMilli {
	return UnixMilli(ts / 1e6)
}

func (ts UnixNano) UnixMicro() UnixMicro {
	return UnixMicro(ts / 1e3)
}

func (ts UnixNano) Time() time.Time {
	return time.Unix(int64(ts)/1e9, int64(ts)%1e9)
}

func (ts UnixNano) MarshalText() ([]byte, error) {
	return []byte(strconv.FormatInt(int64(ts), 10)), nil
}

func (ts *UnixNano) UnmarshalText(data []byte) error {
	val, err := strconv.ParseInt(string(data), 10, 64)
	if err != nil {
		return err
	}

	*ts = UnixNano(val)

	return nil
}
