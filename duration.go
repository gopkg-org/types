/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package types

import "time"

type Duration int64

func (d Duration) Duration() time.Duration {
	return time.Duration(d)
}

func (d Duration) MarshalText() ([]byte, error) {
	return []byte(d.Duration().String()), nil
}

func (d *Duration) UnmarshalText(data []byte) error {
	val, err := time.ParseDuration(string(data))
	if err != nil {
		return err
	}

	*d = Duration(val)

	return nil
}
