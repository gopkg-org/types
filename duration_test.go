/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package types_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gopkg.org/types"
)

func TestDuration_Duration(t *testing.T) {
	t.Run("1h15m30s", func(t *testing.T) {
		assert.Equal(t, (time.Hour*1)+(time.Minute*15)+(time.Second*30), types.Duration(4530000000000).Duration())
	})
}

func TestDuration_MarshalText(t *testing.T) {
	t.Run("1h15m30s", func(t *testing.T) {
		data, err := types.Duration((time.Hour * 1) + (time.Minute * 15) + (time.Second * 30)).MarshalText()
		assert.NoError(t, err)
		assert.Equal(t, []byte("1h15m30s"), data)
	})
}

func TestDuration_UnmarshalText(t *testing.T) {
	t.Run("1h15m30s", func(t *testing.T) {
		var d types.Duration
		assert.NoError(t, d.UnmarshalText([]byte(`1h15m30s`)))
		assert.Equal(t, types.Duration((time.Hour*1)+(time.Minute*15)+(time.Second*30)), d)
	})

	t.Run("missing-unit", func(t *testing.T) {
		var d types.Duration
		assert.Error(t, d.UnmarshalText([]byte(`3600`)))
		assert.Equal(t, types.Duration(0), d)
	})

	t.Run("empty", func(t *testing.T) {
		var d types.Duration
		assert.Error(t, d.UnmarshalText([]byte(``)))
		assert.Equal(t, types.Duration(0), d)
	})

	t.Run("nil", func(t *testing.T) {
		var d types.Duration
		assert.Error(t, d.UnmarshalText(nil))
		assert.Equal(t, types.Duration(0), d)
	})
}
