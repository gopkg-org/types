# Types

This package provides types with Marshaler and Unmarshaler.

## [Base64](./base64.go)

Decode and encode base64 value in `[]byte` with [encoding/base64.StdEncoding](https://pkg.go.dev/encoding/base64#StdEncoding).

## [RawBase64](./base64.go)

Decode and encode base64 value in `[]byte` with [encoding/base64.RawStdEncoding](https://pkg.go.dev/encoding/base64#RawStdEncoding).

## [Duration](./duration.go)

Decode and encode duration value in `int64` for `time.Duration`.

## [Unix](./timestamp.go)

Decode and encode unix seconds timestamp value in `int64` for `time.Time`.

## [UnixMilli](./timestamp.go)

Decode and encode unix milliseconds timestamp value in `int64` for `time.Time`.

## [UnixMicro](./timestamp.go)

Decode and encode unix microseconds timestamp value in `int64` for `time.Time`.

## [UnixNano](./timestamp.go)

Decode and encode unix nanoseconds timestamp value in `int64` for `time.Time`.
