/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package types_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.org/types"
)

func TestBase64_String(t *testing.T) {
	assert.Equal(t, "value", (types.Base64{0x76, 0x61, 0x6c, 0x75, 0x65}).String())
}

func TestBase64_MarshalText(t *testing.T) {
	data, err := types.Base64("value").MarshalText()
	assert.NoError(t, err)
	assert.Equal(t, []byte("dmFsdWU="), data)
}

func TestBase64_UnmarshalText(t *testing.T) {
	var b64 types.Base64
	assert.NoError(t, b64.UnmarshalText([]byte(`dmFsdWU=`)))
	assert.Equal(t, types.Base64("value"), b64)
}

func TestRawBase64_String(t *testing.T) {
	assert.Equal(t, "value", (types.RawBase64{0x76, 0x61, 0x6c, 0x75, 0x65}).String())
}

func TestRawBase64_MarshalText(t *testing.T) {
	data, err := types.RawBase64("value").MarshalText()
	assert.NoError(t, err)
	assert.Equal(t, []byte("dmFsdWU"), data)
}

func TestRawBase64_UnmarshalText(t *testing.T) {
	var b64 types.RawBase64
	assert.NoError(t, b64.UnmarshalText([]byte(`dmFsdWU`)))
	assert.Equal(t, types.RawBase64("value"), b64)
}
