/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package types

import "encoding/base64"

type Base64 []byte

func (b64 Base64) String() string {
	return string(b64)
}

func (b64 Base64) MarshalText() ([]byte, error) {
	dst := make([]byte, base64.StdEncoding.EncodedLen(len(b64)))
	base64.StdEncoding.Encode(dst, b64)
	return dst, nil
}

func (b64 *Base64) UnmarshalText(data []byte) error {
	*b64 = make(Base64, base64.StdEncoding.DecodedLen(len(data)))
	n, err := base64.StdEncoding.Decode(*b64, data)
	*b64 = (*b64)[:n]
	return err
}

type RawBase64 []byte

func (b64 RawBase64) String() string {
	return string(b64)
}

func (b64 RawBase64) MarshalText() ([]byte, error) {
	dst := make([]byte, base64.RawStdEncoding.EncodedLen(len(b64)))
	base64.RawStdEncoding.Encode(dst, b64)
	return dst, nil
}

func (b64 *RawBase64) UnmarshalText(data []byte) error {
	*b64 = make(RawBase64, base64.RawStdEncoding.DecodedLen(len(data)))
	n, err := base64.RawStdEncoding.Decode(*b64, data)
	*b64 = (*b64)[:n]
	return err
}
